#ifndef ARS1_H
#define ARS1_H

#include <QMainWindow>
#include <QTimer>
#include <QDateTime>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>

namespace Ui {
class ars1;
}

class ars1 : public QMainWindow
{
    Q_OBJECT

public:
    explicit ars1(QWidget *parent = 0);
    ~ars1();

private slots:

    void show_clock();

    void write_to_arduino();


private:

    QString time_text;
    QString rc;

    QTime time;

    Ui::ars1 *ui;
    QSerialPort *arduino;
    static const quint16 arduino_leo_vendor_id =9025;
    static const quint16 arduino_leo_product_id=32822;
    QString arduino_port_name;
    bool arduino_is_available;


};

#endif // ARS1_H
