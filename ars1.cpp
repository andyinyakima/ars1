#include "ars1.h"
#include "ui_ars1.h"

ars1::ars1(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ars1)
{
    ui->setupUi(this);

    arduino =new QSerialPort;
    arduino_is_available = false;
    arduino_port_name = "";

    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
        if(serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier()){
            if(serialPortInfo.vendorIdentifier()==arduino_leo_vendor_id){
                if(serialPortInfo.productIdentifier()==arduino_leo_product_id){
                    arduino_port_name = serialPortInfo.portName();
                    arduino_is_available = true;
                   //  ui->usbport_lineEdit->insert(arduino_port_name);
                }
            }
        }

    if(arduino_is_available){
     //  open and configure serial port
       arduino->setPortName(arduino_port_name);
       arduino->open(QSerialPort::WriteOnly);
       arduino->setBaudRate(QSerialPort::Baud9600);
       arduino->setDataBits(QSerialPort::Data8);
       arduino->setParity(QSerialPort::NoParity);
       arduino->setStopBits(QSerialPort::OneStop);
       arduino->setFlowControl(QSerialPort::NoFlowControl);



    }


    QTimer *timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(show_clock()));
    timer->start(1000);



}
ars1::~ars1()
{
    delete ui;
}

void ars1::show_clock()
{
    QString alarm_txt =ui->emit_timeEdit->text();
    time = QTime::currentTime();
    time_text=time.toString("hh:mm:ss");
    ui->Clock->setText(time_text);

    if((time_text == ui->emit_timeEdit->text()) == true)
    {
        rc=ui->remote_signal_lineEdit->text();
        write_to_arduino();
        rc.clear();
    }

}

void ars1::write_to_arduino()
{
    if(arduino->isWritable())
    {
        qDebug()<<rc;
        arduino->write(rc.toStdString().c_str());
        return;
    }
    else{
        ui->remote_signal_lineEdit->clear();
        ui->remote_signal_lineEdit->insert("Couldn't write to Arduino");
    }
}
